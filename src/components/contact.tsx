import React from 'react';

export const Contact = () => {
  return (
    <div id='contact' className='flex items-center justify-center mt-12'>
      <div className='container'>
        <div className='bg-white rounded-lg shadow-lg p-5 md:p-20 mx-2'>
          <div className='text-center'>
            <h2 className='text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:text-5xl sm:leading-none md:text-6xl'>
              Contact<span className='text-pink-500'>Me!</span>
            </h2>
            <h3 className='text-xl md:text-3xl mt-10'>
              These are the differents ways
            </h3>
          </div>
          <div className='flex flex-wrap mt-10 justify-center'>
            <div className='m-3'>
              <a
                target='_blank'
                href='https://www.linkedin.com/in/luzpm1102/'
                className='md:w-32 bg-white tracking-wide text-gray-800 font-bold rounded border-2 border-blue-600 hover:border-blue-600 hover:bg-blue-600 hover:text-white shadow-md py-2 px-6 inline-flex items-center'
              >
                <span className='mx-auto'>LinkedIn</span>
              </a>
            </div>

            <div className='m-3'>
              <a
                href='mailto:luzpm1102@gmail.com'
                target='_blank'
                className='md:w-32 bg-white tracking-wide text-gray-800 font-bold rounded border-2 border-red-600 hover:border-red-600 hover:bg-red-600 hover:text-white shadow-md py-2 px-6 inline-flex items-center'
              >
                <span className='mx-auto'>Gmail</span>
              </a>
            </div>
            <div className='m-3'>
              <a
                href='tel:+50581120047'
                target='_blank'
                className='md:w-32 bg-white tracking-wide text-gray-800 font-bold rounded border-2 border-green-500 hover:border-green-500 hover:bg-green-500 hover:text-white shadow-md py-2 px-6 inline-flex items-center'
              >
                <span className='mx-auto'>Call me</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
