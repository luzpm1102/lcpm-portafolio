import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Card from './card';
import { Tag } from '../Interfaces/Tags';
import { Experience } from './experience';

const About = () => {
  const data = useStaticQuery(graphql`
    query {
      icons: allJsonJson(filter: { title: { eq: "Technologies" } }) {
        edges {
          node {
            title
          }
        }
        nodes {
          content {
            id
            name
            image {
              childImageSharp {
                gatsbyImageData(width: 80, placeholder: BLURRED)
              }
            }
          }
        }
      }
    }
  `);
  const icons: Tag[] = data.icons.nodes[0].content;

  return (
    <div id='about' className='my-6'>
      <div className='lg:flex justify-around'>
        <div className='m-0 lg:w-1/2'>
          <h1 className='text-xl font-bold text-pink-800 text-center uppercase'>
            about me
          </h1>
          <div className='flex justify-center items-center '>
            <hr className='p-0.5 bg-pink-200 w-40 rounded-lg ' />
          </div>
          <div className=' p-5'>
            <p className='text-justify '>
              I'm a capable, responsible, self-taught-person with an eager to
              learn something new every day, I like to give my best in all the
              projects I worked. I'm focus on learning cross-platforms
              technologies
            </p>
          </div>
        </div>
        <div className='m-0'>
          <h1 className='text-xl font-bold text-pink-800 text-center uppercase'>
            Education
          </h1>
          <div className='flex justify-center items-center '>
            <hr className='p-0.5 bg-pink-200 w-40 rounded-lg ' />
          </div>
          <div className=' p-5 text-center'>
            <h2 className=' font-black '>Universidad Americana (UAM) </h2>
            <span>January • 2017 - December • 2021</span>
            <h2 className=' font-black'>TOEIC</h2>
            <span>July • 2021 - July • 2023</span>
            <h2 className=' font-black'>Project Management Course</h2>
            <span> December • 2021 </span>
          </div>
        </div>
      </div>
      <Experience />
      <div className='my-12'>
        <h1 className='text-xl font-bold text-pink-800 text-center uppercase'>
          tecnologies
        </h1>
        <div className='flex justify-center items-center '>
          <hr className='p-0.5 bg-pink-200 w-40 rounded-lg ' />
        </div>
        <div className='mt-5 p-5'>
          <div className='grid md:grid-cols-4 lg:grid-cols-5 grid-cols-2 gap-4 justify-items-center items-center'>
            {icons &&
              icons.map((icon) => (
                <Card img={icon.image} title={icon.name} key={icon.id} />
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
