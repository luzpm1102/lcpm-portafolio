import { ChildImageSharp } from './Images';

export interface Image {
  childImageSharp: ChildImageSharp;
}

export interface Content {
  title: string;
  description: string;
  tags: string[];
  gitRepository: string;
  deployedWeb: string;
  id: number;
  image: Image;
  slug: string;
  layout: 'primary' | 'secondary';
}
export type Edge = {
  node: Content;
};
