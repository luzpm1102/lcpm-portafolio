import { graphql } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';
import React from 'react';
import Layout from '../components/layout';
import { Content } from '../Interfaces/Project';
interface Props {
  data: {
    projectsJson: Content;
  };
}

export const query = graphql`
  query ($slug: String!) {
    projectsJson(slug: { eq: $slug }) {
      jsonId
      title
      slug
      tags
      image {
        childImageSharp {
          gatsbyImageData(width: 700, placeholder: BLURRED)
        }
      }
      description
      deployedWeb
      gitRepository
    }
  }
`;
const ProjectDetailsSecond = ({ data }: Props) => {
  const project = data.projectsJson;
  return (
    <Layout pageTitle='Project Detail'>
      <section className='text-gray-700 body-font overflow-hidden bg-gray-50 '>
        <div className=' px-5 py-24 '>
          <div className='lg:w-full mx-10 flex flex-col md:flex-row items-center'>
            <a
              href='#'
              className='mx-10 transition duration-200 ease-out transform hover:scale-110'
            >
              <GatsbyImage
                image={project.image.childImageSharp.gatsbyImageData}
                alt={project.title}
                loading='lazy'
              />
            </a>
            <div className='flex justify-center flex-col items-center my-4 rounded  '>
              <div className='lg:w-1/2 w-full lg:py-6 mt-6 lg:mt-0 bg-white p-4 border rounded'>
                <h1 className='text-gray-900 text-3xl title-font font-medium mb-1 text-center'>
                  {project.title}
                </h1>
                <div className='flex justify-center mb-4'>
                  <div className='flex items-center justify-start mt-6 mb-4'>
                    {project.tags.map((tag) => (
                      <div
                        key={tag}
                        className='px-2 py-1 font-bold bg-pink-400 text-white rounded-lg hover:bg-gray-500 mr-4'
                      >
                        {' '}
                        {tag}
                      </div>
                    ))}
                  </div>
                </div>
                <p className='leading-relaxed'>{project.description}</p>
                <div className='flex mt-6 items-center pb-5 border-b-2 border-gray-200 mb-5'></div>
                <div className=' grid grid-cols-2'>
                  <a href={project.gitRepository} target='_blank'>
                    <button className='mx-6 text-white bg-pink-500 border-0 py-2 px-6 focus:outline-none hover:bg-pink-600 rounded'>
                      Repository
                    </button>
                  </a>
                  <a href={project.deployedWeb} target='_blank'>
                    <button className='mx-6 text-white bg-pink-500 border-0 py-2 px-6 focus:outline-none hover:bg-pink-600 rounded'>
                      Demo
                    </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default ProjectDetailsSecond;
