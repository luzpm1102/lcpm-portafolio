import { Layout } from 'gatsby-plugin-image';

export interface Placeholder {
  fallback: string;
}

export interface Fallback {
  src: string;
  srcSet: string;
  sizes: string;
}

export interface Source {
  srcSet: string;
  type: string;
  sizes: string;
}

export interface Images {
  fallback: Fallback;
  sources: Source[];
}

export interface GatsbyImageData {
  layout: Layout;
  placeholder: Placeholder;
  images: Images;
  width: number;
  height: number;
}

export interface ChildImageSharp {
  gatsbyImageData: GatsbyImageData;
}

export interface Node {
  childImageSharp: ChildImageSharp;
  name: string;
}

export interface Icon {
  node: Node;
}
