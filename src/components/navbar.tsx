import { Link } from 'gatsby';
import React from 'react';

export const Navbar = () => {
  const path = typeof window !== 'undefined' ? window.location.pathname : '';

  return (
    <nav className='flex justify-between bg-pink-500 p-3  items-center'>
      <Link to='/' className='mr-6 text-white cursor-pointer   '>
        <h1 className='text-xl font-bold text-center'>LCPM</h1>
      </Link>

      <ul className=' flex md:flex-row items-center '>
        <li className='mr-6 text-white cursor-pointer hover:opacity-75 font-bold lg:text-lg sm:text-base'>
          <Link to={path === '/' ? '#about' : '/#about'}>
            <h1 className='text-xl font-bold text-center'>About</h1>
          </Link>
        </li>
        <li className='mr-6 text-white cursor-pointer  hover:opacity-75 font-bold lg:text-lg md:text-base'>
          <Link to={path === '/' ? '#projects' : '/#projects'}>
            <h1 className='text-xl font-bold text-center'>Projects</h1>
          </Link>
        </li>
        <li className='mr-1 text-white cursor-pointer hover:opacity-75 font-bold lg:text-lg md:text-base'>
          <Link to={path === '/' ? '#contact' : '/#contact'}>
            <h1 className='text-xl font-bold text-center'>Contact</h1>
          </Link>
        </li>
      </ul>
    </nav>
  );
};
