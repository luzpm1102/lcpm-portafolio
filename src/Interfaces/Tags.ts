import { ChildImageSharp, GatsbyImageData } from './Images';

export interface Image {
  childImageSharp: ChildImageSharp;
}

export interface Tag {
  id: number;
  name: string;
  image: Image;
}
