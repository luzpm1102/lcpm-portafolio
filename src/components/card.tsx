import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import React from 'react';
import { Image } from '../Interfaces/Tags';

interface Props {
  img: Image;
  title: string;
}

export const Card = ({ img, title }: Props) => {
  return (
    <div className='block transition duration-200 ease-out transform hover:scale-110'>
      <GatsbyImage
        image={img.childImageSharp.gatsbyImageData}
        alt={title}
        loading='lazy'
      />
      <h3 className='uppercase text-center mt-2'>{title}</h3>
    </div>
  );
};

export default Card;
