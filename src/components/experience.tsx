import React from 'react';

export const Experience = () => {
  return (
    <div className='my-12 mx-5' id='experience'>
      <h1 className='text-xl font-bold text-pink-800 text-center uppercase'>
        Experience
      </h1>
      <div className='flex justify-center items-center '>
        <hr className='p-0.5 bg-pink-200 w-40 rounded-lg ' />
      </div>
      <div className=' flex items-center justify-center my-20'>
        <div className='grid md:grid-cols-12 grid-cols-1 max-w-7xl gap-20'>
          <div className='grid col-span-4 relative' id='experience1'>
            <a
              className='group shadow-lg hover:shadow-2xl duration-200 delay-75 w-full bg-white rounded-sm py-6 pr-6 pl-9'
              href='#experience1'
            >
              <p className='text-2xl font-bold text-gray-500 group-hover:text-gray-700 '>
                {' '}
                Applaudo Studios
              </p>
              <small>React Trainee</small>
              <br />
              <small>February - 2022</small>

              <p className='text-sm font-semibold text-gray-500 group-hover:text-gray-700 mt-2 leading-6 text-justify'>
                Learned the basics of react, how to create reusable components,
                hooks, redux, and differents technologies I could use with
                react, also about react testing with react testing library and
                Jest
              </p>

              <div className='bg-pink-400 group-hover:bg-pink-600 h-full w-4 absolute top-0 left-0'>
                {' '}
              </div>
            </a>
          </div>

          <div className='grid col-span-4 relative' id='experience2'>
            <a
              className='group shadow-lg hover:shadow-2xl duration-200 delay-75 w-full bg-white rounded-sm py-6 pr-6 pl-9'
              href='#experience2'
            >
              <p className='text-2xl font-bold text-gray-500 group-hover:text-gray-700'>
                {' '}
                GBM
              </p>
              <small>Software Specialist</small>
              <br />
              <small>April - 2022 March - 2022</small>
              <p className='text-sm font-semibold text-gray-500 group-hover:text-gray-700 mt-2 leading-6 text-justify'>
                I took a course about react native, learned how to use react
                native and develop for iOS and Android, also gain an
                understanding of how SCRUM works and apply this framework to my
                daily working day
              </p>

              <div className='bg-pink-400 group-hover:bg-pink-600 h-full w-4 absolute top-0 left-0'>
                {' '}
              </div>
            </a>
          </div>

          <div className='grid col-span-4   relative' id='experience3'>
            <a
              className='group shadow-lg hover:shadow-2xl duration-200 delay-75 w-full bg-white rounded-sm py-6 pr-6 pl-9'
              href='#experience3'
            >
              <p className='text-2xl font-bold text-gray-500 group-hover:text-gray-700'>
                {' '}
                Sima-ITS
              </p>
              <small>QA Tester</small>
              <br />
              <small>June - 2020 April - 2021</small>

              <p className='text-sm font-semibold text-gray-500 group-hover:text-gray-700 mt-2 leading-6 text-justify'>
                I learned how to do functional testing in web and mobile
                applications, also elaborate this results in a informative way,
                and its users manual
              </p>

              <div className='bg-pink-400 group-hover:bg-pink-600 h-full w-4 absolute top-0 left-0'>
                {' '}
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};
