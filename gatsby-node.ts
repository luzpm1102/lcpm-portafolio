import { Edge } from './src/Interfaces/Project';
import path from 'path';
import { GatsbyNode } from 'gatsby';

type TypeData = {
  allProjectsJson: {
    edges: Edge[];
  };
};

export const createPages: GatsbyNode['createPages'] = async ({
  graphql,
  actions,
}) => {
  const { createPage } = actions;
  const data = await graphql<TypeData>(`
    {
      allProjectsJson {
        edges {
          node {
            slug
            layout
          }
        }
      }
    }
  `);

  if (data.errors) {
    console.log('Error retrieving data', data.errors);
    return;
  }

  const projectsTemplate = path.resolve('./src/templates/ProjectDetails.tsx');
  const projectsTemplateSecond = path.resolve(
    './src/templates/ProjectDetailsSecond.tsx'
  );

  data.data?.allProjectsJson.edges.forEach((edge: Edge) => {
    createPage({
      path: `/project/${edge.node.slug}/`,
      component:
        edge.node.layout === 'primary'
          ? projectsTemplate
          : projectsTemplateSecond,
      context: {
        slug: edge.node.slug,
      },
    });
  });
};
