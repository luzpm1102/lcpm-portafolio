import { graphql } from 'gatsby';
import React from 'react';
import Layout from '../components/layout';
import { Content } from '../Interfaces/Project';
import { GatsbyImage } from 'gatsby-plugin-image';

interface Props {
  data: {
    projectsJson: Content;
  };
}

export const query = graphql`
  query ($slug: String!) {
    projectsJson(slug: { eq: $slug }) {
      jsonId
      title
      slug
      tags
      image {
        childImageSharp {
          gatsbyImageData(width: 800, placeholder: BLURRED)
        }
      }
      description
      deployedWeb
      gitRepository
    }
  }
`;

const ProjectDetails = ({ data }: Props) => {
  const project = data.projectsJson;

  return (
    <Layout pageTitle='DetailPage'>
      <div className='mt-6 bg-gray-50'>
        <div className=' px-10 py-6 mx-auto'>
          <div className='max-w-6xl px-10 py-6 mx-auto bg-gray-50 '>
            <div className='flex justify-center'>
              <a
                href='#'
                className='block transition duration-200 ease-out transform hover:scale-110'
              >
                <GatsbyImage
                  image={project.image.childImageSharp.gatsbyImageData}
                  alt={project.title}
                  loading='lazy'
                />
              </a>
            </div>

            <div className='flex items-center justify-start mt-6 mb-4'>
              {project.tags.map((tag) => (
                <div
                  key={tag}
                  className='px-2 py-1 font-bold bg-pink-400 text-white rounded-lg hover:bg-gray-500 mr-4'
                >
                  {' '}
                  {tag}
                </div>
              ))}
            </div>
            <div className='mt-2'>
              <h2 className='sm:text-3xl md:text-3xl lg:text-3xl xl:text-4xl font-bold text-pink-500  '>
                {project.title}
              </h2>
            </div>

            <div className='max-w-full mx-auto md:text-xl text-gray-700 mt-4 rounded bg-gray-100'>
              <div>
                <p className='mt-2 p-8'>{project.description}</p>
              </div>
            </div>
          </div>
          <div className=' flex justify-center items-center'>
            <a href={project.gitRepository} target='_blank'>
              <button className='mx-6 text-white bg-pink-500 border-0 py-2 px-6 focus:outline-none hover:bg-pink-600 rounded'>
                Repository
              </button>
            </a>
            <a href={project.deployedWeb} target='_blank'>
              <button className='mx-6 text-white bg-pink-500 border-0 py-2 px-6 focus:outline-none hover:bg-pink-600 rounded'>
                Demo
              </button>
            </a>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ProjectDetails;
