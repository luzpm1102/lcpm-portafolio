import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const Main = () => {
  return (
    <div className=' bg-pink-500'>
      <div className='flex sm:flex-row flex-col justify-between sm:justify-around items-center mx-5 '>
        <div>
          <h3 className='text-white italic text-xl mt-5 '>Hi, I'm Luz Perez</h3>
          <h1 className='text-white italic text-xl font-black'>
            FRONTEND DEVELOPER
          </h1>
        </div>
        <StaticImage
          alt='woman'
          src='../images/woman.png'
          className='sm:w-80 md:w-5/12'
          loading='lazy'
        />
      </div>
    </div>
  );
};

export default Main;
