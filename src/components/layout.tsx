import * as React from 'react';
import { Navbar } from './navbar';
import { Link, useStaticQuery, graphql } from 'gatsby';
interface Props {
  pageTitle: string;
  children: React.ReactNode;
}

const Layout = ({ pageTitle, children }: Props) => {
  const data = useStaticQuery(graphql`
    query {
      site(siteMetadata: {}) {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <div>
      <title>
        {pageTitle} | {data.site.siteMetadata.title}
      </title>

      <Navbar />
      <main>{children}</main>
    </div>
  );
};
export default Layout;
