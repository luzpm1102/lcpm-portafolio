import { StaticImage } from 'gatsby-plugin-image';
import * as React from 'react';
import About from '../components/about';
import { Contact } from '../components/contact';
import Layout from '../components/layout';
import Main from '../components/main';
import { Projects } from '../components/projects';
const IndexPage = () => {
  return (
    <Layout pageTitle='HomePage'>
      <Main />
      <div className='my-10' />
      <About />
      <div className='my-10' />
      <Projects />
      <Contact />
    </Layout>
  );
};
export default IndexPage;
