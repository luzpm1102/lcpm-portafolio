import { graphql, Link, navigate, useStaticQuery } from 'gatsby';
import React, { useState } from 'react';
import { Edge, Content } from '../Interfaces/Project';
import { GatsbyImage } from 'gatsby-plugin-image';

export const Projects = () => {
  const data = useStaticQuery(graphql`
    {
      projects: allProjectsJson {
        edges {
          node {
            jsonId
            slug
            tags
            title
            description
            gitRepository
            deployedWeb
            image {
              childImageSharp {
                gatsbyImageData(width: 800, placeholder: BLURRED)
              }
            }
          }
        }
      }
      tags: allJsonJson(filter: { title: { eq: "Technologies" } }) {
        edges {
          node {
            content {
              id
              name
            }
          }
        }
      }
    }
  `);

  const initialState = data.projects.edges;
  const tags: { id: number; name: string }[] = data.tags.edges[0].node.content;

  const [projects, setProjects] = useState<Edge[]>(initialState);

  const filter = (value: string) => {
    if (value === 'All') {
      setProjects(initialState);
      return;
    }
    setProjects(
      initialState.filter((element: Edge) => element.node.tags.includes(value))
    );
  };
  return (
    <div id='projects' className='my-14'>
      <h1 className='text-xl font-bold text-pink-800 text-center uppercase'>
        Projects
      </h1>
      <div className='flex justify-center items-center '>
        <hr className='p-0.5 bg-pink-200 w-40 rounded-lg ' />
      </div>
      <div className='flex justify-center my-12   '>
        <select
          name='filter'
          className='mt-2 w-40 p-2 border rounded-lg focus:outline-none focus:ring-2 focus:ring-pink-400 focus:border-transparent '
          id='filter'
          onChange={(e) => filter(e.target.value)}
        >
          <option value='All'>All</option>
          {tags.map((tag) => (
            <option key={tag.id} value={tag.name}>
              {tag.name}
            </option>
          ))}
        </select>
      </div>
      <div className='grid md:grid-cols-2 lg:grid-cols-3 grid-cols-1 gap-10  justify-items-center items-center p-5 my-12 '>
        {projects.map((project) => (
          <div
            key={project.node.slug}
            className='max-w-xs mx-auto overflow-hidden bg-white rounded-lg shadow-lg border transition duration-200 ease-out transform hover:scale-110'
          >
            <Link to={`/project/${project.node.slug}`}>
              <div className='px-4 py-2 '>
                <h1 className='text-xl font-bold text-gray-800 uppercase my-2 '>
                  {project.node.title}
                </h1>

                {project.node.tags.map((tag, index) => (
                  <span
                    className='px-3 py-1 text-xs text-pink-800 uppercase bg-pink-200 rounded-full mr-1 '
                    key={index}
                  >
                    {tag}
                  </span>
                ))}
              </div>
              <GatsbyImage
                image={project.node.image.childImageSharp.gatsbyImageData}
                alt={project.node.title}
                loading='lazy'
              />
            </Link>
            <div className='flex items-center justify-between px-4 py-2 bg-pink-500'>
              <a href={project.node.gitRepository} target='_blank'>
                <button className='px-2 py-1 text-xs font-semibold text-gray-900 uppercase transition-colors duration-200 transform bg-white rounded hover:bg-gray-200 focus:bg-gray-400 focus:outline-none'>
                  Repository
                </button>
              </a>
              <a href={project.node.deployedWeb} target='_blank'>
                <button className='px-2 py-1 text-xs font-semibold text-gray-900 uppercase transition-colors duration-200 transform bg-white rounded hover:bg-gray-200 focus:bg-gray-400 focus:outline-none'>
                  Demo
                </button>
              </a>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
